from django_contract_manager import settings
from django_mdat_customer.django_mdat_customer.models import *


def get_customers_for_billing():
    customers = list(
        MdatCustomers.objects.filter(created_by_id=settings.MDAT_ROOT_CUSTOMER_ID)
    )

    return customers
