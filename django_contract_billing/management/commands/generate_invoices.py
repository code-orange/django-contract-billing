from calendar import monthrange
from datetime import timedelta
from pprint import pprint

import pythoncom
import win32com.client
from django.conf import settings
from django.core.management.base import BaseCommand

from django_contract_manager_models.django_contract_manager_models.models import *


class Command(BaseCommand):
    help = "Create new invoices from contract database"

    invoices = dict()

    bundles = dict()

    now = date.today() + timedelta(
        days=settings.CONTRACT_MANAGER_BILLING_DAYS_IN_FUTURE
    )

    def monthdelta(self, d1, d2):
        delta = 0
        while True:
            mdays = monthrange(d1.year, d1.month)[1]
            d1 += timedelta(days=mdays)
            if d1 <= d2:
                delta += 1
            else:
                break
        return delta

    def get_invoicing_hints(self, contract):
        data = dict()
        status = dict()

        billed_next_day = contract.billed_until + timedelta(days=1)

        if contract.billed_until.day == 26 and contract.billing_cycle.id == 1:
            print(
                "!!!!!!!!!!!!!!!!!!!! DEBUG: Full month " + contract.billing_cycle.name
            )
            status["first_month"] = "full-" + contract.billing_cycle.name
        elif contract.billing_cycle.id == 2:
            print(
                "!!!!!!!!!!!!!!!!!!!! DEBUG: Full month " + contract.billing_cycle.name
            )
            status["first_month"] = "full-" + contract.billing_cycle.name
        elif contract.billing_cycle.id == 3:
            print(
                "!!!!!!!!!!!!!!!!!!!! DEBUG: Full year " + contract.billing_cycle.name
            )
            status["first_month"] = "full-" + contract.billing_cycle.name
        else:
            print(
                "!!!!!!!!!!!!!!!!!!!! DEBUG: Partly month "
                + contract.billing_cycle.name
            )
            status["first_month"] = "partly-" + contract.billing_cycle.name

        if contract.billing_cycle.id == 1:
            if contract.billed_until.month == 12 and contract.billed_until.day >= 26:
                billed_next_month = contract.billed_until.replace(
                    day=26, month=1, year=contract.billed_until.year + 1
                )
            elif contract.billed_until.day >= 26:
                billed_next_month = contract.billed_until.replace(
                    day=26, month=contract.billed_until.month + 1
                )
            else:
                billed_next_month = contract.billed_until.replace(
                    day=26, month=contract.billed_until.month
                )
        elif contract.billing_cycle.id == 2:
            if contract.billed_until.month == 11:
                # muss auch schon gesplittet werden, weil sonst 11 + 2 Monate = 13 -> Exception
                billed_next_month = contract.billed_until.replace(
                    day=1, month=1, year=contract.billed_until.year + 1
                )
                billed_next_month = billed_next_month - timedelta(days=1)
            elif contract.billed_until.month == 12:
                billed_next_month = contract.billed_until.replace(
                    day=1, month=2, year=contract.billed_until.year + 1
                )
                billed_next_month = billed_next_month - timedelta(days=1)
            else:
                billed_next_month = contract.billed_until.replace(
                    day=1, month=contract.billed_until.month + 2
                )
                billed_next_month = billed_next_month - timedelta(days=1)
        elif contract.billing_cycle.id == 3:
            billed_next_month = contract.billed_until.replace(
                year=contract.billed_until.year + 1
            )
        else:
            raise ValueError("Unknown invoicing cycle!")

        print(
            "!!!!!!!!!!!!!!!!!!!! DEBUG: Current Date: "
            + billed_next_day.strftime("%d.%m.%Y")
            + " - Next Month: "
            + billed_next_month.strftime("%d.%m.%Y")
        )

        if contract.end is not None:
            if billed_next_month > contract.end:
                billed_next_month = contract.end

        data["billed_until_end_of_contract"] = False

        if status["first_month"] == "partly-month-on-26":
            pay_diff = billed_next_month - billed_next_day
            data["quantity"] = pay_diff.days / 30
        elif status["first_month"] == "full-month-on-26":
            if contract.end is not None:
                diff_month = self.monthdelta(billed_next_day, contract.end)
                print("Diff Month" + str(diff_month))

                if True:  # contract.u_cancelcycle == 0 and diff_month > 1:
                    # billing monthly
                    data["quantity"] = 1
                else:
                    # billing all at once
                    # TODO: Fix contracts with dates in following year
                    temp_until = contract.end - billed_next_day.replace(
                        month=billed_next_day.month + diff_month
                    )
                    diff_day = temp_until.days / 30
                    print("Diff Day" + str(diff_day))

                    if diff_day < 0:
                        # contract was canceled before billed_until
                        data["quantity"] = 0
                    else:
                        data["quantity"] = diff_month + diff_day

                    data["billed_until_end_of_contract"] = True
                    billed_next_month = contract.end
            else:
                data["quantity"] = 1
        elif status["first_month"] == "full-full-month":
            if contract.end is not None:
                quantity = self.monthdelta(billed_next_day, contract.end) + 1

                if True:  # contract.u_cancelcycle == 0 and quantity > 1:
                    # billing monthly
                    data["quantity"] = 1
                else:
                    # billing all at once
                    data["quantity"] = quantity
                    data["billed_until_end_of_contract"] = True
                    billed_next_month = contract.end

            else:
                data["quantity"] = 1
        elif status["first_month"] == "full-full-year":
            data["quantity"] = 1
        else:
            data["quantity"] = 0

        data["billed_next_month"] = billed_next_month
        data["billed_next_day"] = billed_next_day

        return data

    def traverse_contract(self, contract: Contracts):
        print(contract.item.external_id)

        # kundennummer, contract, billed until, artnr, preis, rabatt, quantity + 1

        if contract.customer.external_id not in self.bundles:
            # kundennummer
            self.bundles[contract.customer.external_id] = dict()

        if contract.customer.external_id not in self.invoices:
            # kundennummer
            self.invoices[contract.customer.external_id] = dict()

        product_bundles = ContractBundles.objects.filter(
            item_src=contract.item
        ).order_by("item_dst")

        if contract.master_contract is None:
            contract_id = contract.id
        else:
            contract_id = contract.master_contract.id

        # vertragsnummer
        if str(contract_id) not in self.invoices[contract.customer.external_id]:
            self.invoices[contract.customer.external_id][str(contract_id)] = dict()

        if str(contract_id) not in self.bundles[contract.customer.external_id]:
            self.bundles[contract.customer.external_id][str(contract_id)] = dict()

        while (
            contract.billed_until < self.now and contract.billed_until != contract.end
        ):
            # Zeitraum festlegen bis wann abgerechnet wird
            contract_info = self.get_invoicing_hints(contract)

            contract_dateend = "none"
            if contract.end is not None:
                contract_dateend = contract.end.strftime("%Y-%m-%d")

            print(
                contract.billed_until.strftime("%Y-%m-%d")
                + " < "
                + self.now.strftime("%Y-%m-%d")
                + " VS "
                + contract_info["billed_next_month"].strftime("%Y-%m-%d")
            )
            # billed until
            if (
                contract.billed_until.strftime("%Y-%m-%d")
                not in self.invoices[contract.customer.external_id][str(contract_id)]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ] = dict()

            if (
                contract.billed_until.strftime("%Y-%m-%d")
                not in self.bundles[contract.customer.external_id][str(contract_id)]
            ):
                self.bundles[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ] = dict()

            # date end
            if (
                contract_dateend
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend] = dict()

            for bundle in product_bundles:
                # bundle quantity
                if (
                    bundle.item_dst.external_id
                    not in self.bundles[contract.customer.external_id][
                        str(contract_id)
                    ][contract.billed_until.strftime("%Y-%m-%d")]
                ):
                    self.bundles[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][bundle.item_dst.external_id] = dict()

                if (
                    "quantity"
                    not in self.bundles[contract.customer.external_id][
                        str(contract_id)
                    ][contract.billed_until.strftime("%Y-%m-%d")][
                        bundle.item_dst.external_id
                    ]
                ):
                    self.bundles[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][bundle.item_dst.external_id]["quantity"] = bundle.quantity
                else:
                    current_quantity = self.bundles[contract.customer.external_id][
                        str(contract_id)
                    ][contract.billed_until.strftime("%Y-%m-%d")][
                        bundle.item_dst.external_id
                    ][
                        "quantity"
                    ]
                    self.bundles[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][bundle.item_dst.external_id]["quantity"] = (
                        current_quantity + bundle.quantity
                    )

            # artnr
            if (
                contract.item.external_id
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id] = dict()

            # preis
            contract_price = contract.item.price

            if contract.price > contract.item.price:
                contract_price = contract.price

            if (
                contract_price
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price] = dict()

            # rabatt
            if (
                contract.discount_percent
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ] = dict()

            # Zeitraum bis wann abgerechnet wird, setzen.
            self.invoices[contract.customer.external_id][str(contract_id)][
                contract.billed_until.strftime("%Y-%m-%d")
            ][contract_dateend][contract.item.external_id][contract_price][
                contract.discount_percent
            ][
                "new_billed_from_date"
            ] = contract_info[
                "billed_next_day"
            ]

            self.invoices[contract.customer.external_id][str(contract_id)][
                contract.billed_until.strftime("%Y-%m-%d")
            ][contract_dateend][contract.item.external_id][contract_price][
                contract.discount_percent
            ][
                "new_billed_until_date"
            ] = contract_info[
                "billed_next_month"
            ]

            # contracts
            if (
                "contracts"
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ][
                    "contracts"
                ] = list()

            self.invoices[contract.customer.external_id][str(contract_id)][
                contract.billed_until.strftime("%Y-%m-%d")
            ][contract_dateend][contract.item.external_id][contract_price][
                contract.discount_percent
            ][
                "contracts"
            ].append(
                contract
            )

            # quantity
            if (
                "quantity"
                not in self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ]
            ):
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ][
                    "quantity"
                ] = 0

            current_quantity = self.invoices[contract.customer.external_id][
                str(contract_id)
            ][contract.billed_until.strftime("%Y-%m-%d")][contract_dateend][
                contract.item.external_id
            ][
                contract_price
            ][
                contract.discount_percent
            ][
                "quantity"
            ]

            if (
                contract.item.external_id
                in self.bundles[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ]
                and contract_info["billed_until_end_of_contract"] is False
            ):
                print("XXXXXXXXXXXX ITEMCODE IN BUNDLE")
                if (
                    not self.bundles[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][contract.item.external_id]["quantity"]
                    > 0
                ):
                    self.invoices[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][contract_dateend][contract.item.external_id][contract_price][
                        contract.discount_percent
                    ][
                        "quantity"
                    ] = (
                        current_quantity + contract_info["quantity"]
                    )
                else:
                    self.bundles[contract.customer.external_id][str(contract_id)][
                        contract.billed_until.strftime("%Y-%m-%d")
                    ][contract.item.external_id]["quantity"] = (
                        self.bundles[contract.customer.external_id][str(contract_id)][
                            contract.billed_until.strftime("%Y-%m-%d")
                        ][contract.item.external_id]["quantity"]
                        - 1
                    )

            else:
                self.invoices[contract.customer.external_id][str(contract_id)][
                    contract.billed_until.strftime("%Y-%m-%d")
                ][contract_dateend][contract.item.external_id][contract_price][
                    contract.discount_percent
                ][
                    "quantity"
                ] = (
                    current_quantity + contract_info["quantity"]
                )

            contract.billed_until = contract_info["billed_next_month"]

        all_subcontracts = contract.subcontracts.all().order_by("item")

        if all_subcontracts.count():
            for subcontract in all_subcontracts:
                self.traverse_contract(subcontract)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_customers = MdatCustomers.objects.filter(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID
        ).order_by("external_id")

        # SAP Connect
        pythoncom.CoInitialize()

        print("Dispatching COM-Connection")
        vCompany = win32com.client.Dispatch("SAPBobsCOM.Company")
        vCompany.Server = settings.SBO_COMPANY_DATABASE_HOSTNAME
        vCompany.LicenseServer = settings.SBO_COMPANY_LICENSE_SERVER
        vCompany.CompanyDB = settings.SBO_COMPANY_DATABASE_NAME
        vCompany.DbServerType = settings.SBO_COMPANY_DATABASE_VERSION
        vCompany.UserName = settings.SBO_COMPANY_USER
        vCompany.Password = settings.SBO_COMPANY_PASSWORD
        vCompany.UseTrusted = settings.SBO_COMPANY_DATABASE_TRUSTED

        print("Connecting to SAP")
        rtn_cde_connect = vCompany.Connect()

        if rtn_cde_connect:
            print("Unable to connect to SAP: Code " + str(rtn_cde_connect))

            if rtn_cde_connect == 100000048:
                print("No User License")

            if rtn_cde_connect == 2:
                print(
                    "Reboot Computer and delete %temp%\SM_OBS_DLL\ folder and try again"
                )

            if rtn_cde_connect == 14001:
                print("unknown error - delete %temp%\SM_OBS_DLL\ folder and try again")

            if rtn_cde_connect == -10:
                print("NoMsgError - delete %temp%\SM_OBS_DLL\ folder and try again")

            pprint(vCompany.GetLastError())
            pprint(vCompany.GetLastErrorDescription())

            exit(rtn_cde_connect)

        print("Company: " + vCompany.CompanyName)

        for customer in all_customers:
            # skip demo customer
            if customer.external_id == "176511":
                continue

            all_contracts = (
                customer.contracts_set.exclude(master_contract__isnull=False)
                .exclude(item__external_id="DOLSVCBASIC")
                .order_by("item__external_id")
                .select_related()
            )

            if all_contracts.count():
                print("Contract found for " + customer.name + ":")

                for master_contract in all_contracts:
                    print("Neuer Master-Vertrag:")
                    self.traverse_contract(master_contract)

            if customer.external_id not in self.invoices:
                continue

            # SAP CODE
            for main_contract, billed_until_old in self.invoices[
                customer.external_id
            ].items():
                for article_key, article in sorted(billed_until_old.items()):
                    vInv = vCompany.GetBusinessObject(13)
                    vInv.CardCode = customer.external_id
                    vInv.SalesPersonCode = 4

                    vInv.Comments = "Vertrag " + str(main_contract)

                    startdate = datetime.strptime(article_key, "%Y-%m-%d")

                    inv_docdate = startdate
                    inv_taxdate = startdate

                    # TODO: Fix due date if invoice is sent in advance
                    # inv_duedate = startdate

                    vInv.DocDate = inv_docdate.strftime("%Y-%m-%d")
                    vInv.TaxDate = inv_taxdate.strftime("%Y-%m-%d")
                    # vInv.DocDueDate = inv_duedate.strftime('%Y-%m-%d')

                    for cancel_key, cancel_date in article.items():
                        for price_key, price in cancel_date.items():
                            for discount_key, discount in price.items():
                                for row_key, row in discount.items():
                                    # This is the actual row
                                    vInv.Lines.ItemCode = price_key

                                    if row["quantity"] >= 0:
                                        vInv.Lines.Quantity = row["quantity"]
                                    else:
                                        vInv.Lines.Quantity = 0

                                    vInv.Lines.UnitPrice = discount_key

                                    vInv.Lines.DiscountPercent = row_key

                                    vInv.Lines.Currency = "Eur"

                                    row_line_number = vInv.Lines.LineNum

                                    rtn_cde_normal_line_add = vInv.Lines.Add()

                                    if rtn_cde_normal_line_add:
                                        print(
                                            "!!!!!!!!!!!!!!!!!!!! Invoice First Line Return Code: "
                                            + str(rtn_cde_normal_line_add)
                                        )
                                        pprint(vCompany.GetLastError)
                                        raise ValueError(
                                            "Adding Invoice First Line failed..."
                                        )
                                    else:
                                        pass
                                        # contract.billed_until = quantity_data['billed_next_month']
                                        # contract.save()

                                    row_text = (
                                        "Zeitraum: "
                                        + row["new_billed_from_date"].strftime(
                                            "%d.%m.%Y"
                                        )
                                        + " bis "
                                        + row["new_billed_until_date"].strftime(
                                            "%d.%m.%Y"
                                        )
                                        + "\r\n"
                                    )

                                    for contract in row["contracts"]:
                                        if contract.reference is not None:
                                            cust_ref = contract.reference
                                        else:
                                            cust_ref = "keine"

                                        row_text = (
                                            row_text
                                            + "Vertragsnummer: "
                                            + str(contract.id)
                                        )

                                        if contract.end is not None:
                                            row_text = (
                                                row_text
                                                + ", Gekündigt zum: "
                                                + contract.end.strftime("%d.%m.%Y")
                                            )

                                        row_text = (
                                            row_text
                                            + ", Eigene Referenz: "
                                            + cust_ref
                                            + "\r\n"
                                        )

                                        contract.billed_until = row[
                                            "new_billed_until_date"
                                        ]
                                        contract.save()

                                    vInv.SpecialLines.LineType = 0
                                    vInv.SpecialLines.LineText = row_text
                                    vInv.SpecialLines.AfterLineNumber = row_line_number
                                    rtn_cde_special_line_add = vInv.SpecialLines.Add()

                                    if rtn_cde_special_line_add:
                                        print(
                                            "!!!!!!!!!!!!!!!!!!!! Invoice First Line Return Code: "
                                            + str(rtn_cde_special_line_add)
                                        )
                                        pprint(vCompany.GetLastError)
                                        raise ValueError(
                                            "Adding Invoice First Line failed..."
                                        )
                                    else:
                                        pass
                                        # contract.billed_until = quantity_data['billed_next_month']
                                        # contract.save()

                    doc_date_now = date.today()
                    vInv.DocDate = doc_date_now.strftime("%Y-%m-%d")
                    vInv.TaxDate = doc_date_now.strftime("%Y-%m-%d")

                    rtn_cde_inv_add = vInv.Add()

                    if rtn_cde_inv_add:
                        print(
                            "!!!!!!!!!!!!!!!!!!!! Invoice Return Code: "
                            + str(rtn_cde_inv_add)
                        )
                        pprint(vCompany.GetLastError())
                        pprint(vCompany.GetLastErrorDescription())
                        if rtn_cde_inv_add == -5002:
                            print("The Posting Period is closed")
                            print(
                                "The Posting Period Form is located in Administration --> System Initialization --> Posting Period"
                            )
                        raise ValueError("Adding Invoice failed...")

            # pprint(self.invoices)

            # print(json.dumps(self.invoices, ensure_ascii=False, sort_keys=True, indent=4))

            self.invoices.pop(customer.external_id, None)

        vCompany.Disconnect()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
