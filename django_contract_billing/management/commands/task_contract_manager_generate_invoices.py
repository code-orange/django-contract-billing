from django.core.management.base import BaseCommand

from django_contract_billing.django_contract_billing.tasks import (
    contract_manager_generate_invoices,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        contract_manager_generate_invoices()
