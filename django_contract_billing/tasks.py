from celery import shared_task
from django.conf import settings

from django_contract_manager_models.django_contract_manager_models.models import (
    ContractTypes,
)
from django_mdat_customer.django_mdat_customer.models import MdatCustomers


@shared_task(name="contract_manager_generate_invoice_for_customer_and_contract_type")
def contract_manager_generate_invoice_for_customer_and_contract_type(
    mdat_customer_id: int, contract_type_id: int
):
    customer = MdatCustomers.objects.get(id=mdat_customer_id)
    contract_type = ContractTypes.objects.get(id=contract_type_id)

    return


@shared_task(name="contract_manager_generate_invoices_for_customer")
def contract_manager_generate_invoices_for_customer(mdat_customer_id: int):
    for contract_type in ContractTypes.objects.all():
        contract_manager_generate_invoice_for_customer_and_contract_type(
            mdat_customer_id, contract_type.id
        )

    return


@shared_task(name="contract_manager_generate_invoices")
def contract_manager_generate_invoices():
    root_customer = MdatCustomers.objects.get(id=settings.MDAT_ROOT_CUSTOMER_ID)

    for customer in root_customer.get_reseller_customers_and_me():
        contract_manager_generate_invoices_for_customer(customer.id)

    return
